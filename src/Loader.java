import java.io.PrintWriter;

public class Loader {
    public static void main(String[] args) throws Exception {


        PrintWriter writer = new PrintWriter("res/numbers.txt");
        PrintWriter writer2 = new PrintWriter("res/numbers2.txt");
        PrintWriter writer3 = new PrintWriter("res/numbers3.txt");


        StringBuffer buffer = new StringBuffer();
        StringBuffer buffer2 = new StringBuffer();
        StringBuffer buffer3 = new StringBuffer();
        new Thread(() -> writeInFile(writer, buffer, 0, 3)).start();
        new Thread(() -> writeInFile(writer2, buffer2, 3, 6)).start();
        new Thread(() -> writeInFile(writer3, buffer3, 6, 10)).start();
    }

    private static String padNumber(int number, int numberLength) {
        StringBuilder builder = new StringBuilder(Integer.toString(number));
        int padSize = numberLength - builder.length();
        for (int i = 0; i < padSize; i++) {
            builder.insert(0, '0');
        }
        return builder.toString();
    }

    public static void writeInFile(PrintWriter writer, StringBuffer buffer, int codeStart, int codeFinish) {
        long start = System.currentTimeMillis();
        char[] letters = {'У', 'К', 'Е', 'Н', 'Х', 'В', 'А', 'Р', 'О', 'С', 'М', 'Т'};
        for (int regionCode = codeStart; regionCode < codeFinish; regionCode++) {
            for (int number = 1; number < 1000; number++) {
                for (char firstLetter : letters) {
                    for (char secondLetter : letters) {
                        for (char thirdLetter : letters) {
                            buffer.append(firstLetter)
                                    .append(padNumber(number, 3))
                                    .append(secondLetter)
                                    .append(thirdLetter)
                                    .append(padNumber(regionCode, 2))
                                    .append("\n");
                        }
                    }
                }
            }
            writer.write(buffer.toString());
        }
        writer.flush();
        writer.close();
        System.out.println((System.currentTimeMillis() - start) + " ms");
    }
}
